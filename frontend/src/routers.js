import React from "react";
import Questions from "./components/Questions";
import Answers from "./components/Answers";

const routes = {
    "/": () => <Questions />,
    "/results": () => <Answers />
};

export default routes;