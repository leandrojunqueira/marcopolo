import React from "react";
import { Route, BrowserRouter as Router } from "react-router-dom";
import './App.css';
import Questions from "./components/Questions";
import Answers from "./components/Answers";

function App() {
  return (
      <div className="App">
          <Router>
              <div>
                  <Route exact path="/" component={Questions} />
                  <Route exact path="/results" component={Answers} />
              </div>
          </Router>
      </div>
  );
}

export default App;
