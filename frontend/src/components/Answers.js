import React, { useState, useEffect } from "react";
import {getData} from "../ApiHelper";

export default function Answers() {
    const [data, setData] = useState([]);

    useEffect(() => {
        getData().then(response => {
            setData(response);
        });
    }, []);

    const colors = (i) => {

        let color = ['green', 'yellow', 'red'];

        return (
            <>
                {color.map((item, index) => {
                    return <>
                        <td key={index}><span className="span">1</span></td>
                    </>
                })}
                <td><input type="text" name="comment" value=""/></td>
            </>
        )
    }

    return (
        <>
            <table className="table">
                <thead>
                <tr>
                    <th></th>
                    <th><buton type="button" className="green">Green</buton></th>
                    <th><buton type="button" className="yellow">Yellow</buton></th>
                    <th><buton type="button" className="red">Red</buton></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <thead>
                    <tr><th>Delivering Value</th></tr>
                    </thead>
                    {colors()}
                </tr>
                <tr>
                    <thead>
                    <tr><th>Easy Release</th></tr>
                    </thead>
                    {colors()}
                </tr>
                <tr>
                    <thead>
                    <tr><th>Fun</th></tr>
                    </thead>
                    {colors()}
                </tr>
                <tr>
                    <thead>
                    <tr><th>Code Health</th></tr>
                    </thead>
                    {colors()}
                </tr>
                <tr>
                    <thead>
                    <tr><th>Learning</th></tr>
                    </thead>
                    {colors()}
                </tr>
                <tr>
                    <thead>
                    <tr><th>Vision/Mission</th></tr>
                    </thead>
                    {colors()}
                </tr>
                <tr>
                    <thead>
                    <tr><th>Pawns/Players</th></tr>
                    </thead>
                    {colors()}
                </tr>
                <tr>
                    <thead>
                    <tr><th>Speed</th></tr>
                    </thead>
                    {colors()}
                </tr>
                <tr>
                    <thead>
                    <tr><th>Suitable Process</th></tr>
                    </thead>
                    {colors()}
                </tr>
                <tr>
                    <thead>
                    <tr><th>Support</th></tr>
                    </thead>
                    {colors()}
                </tr>
                <tr>
                    <thead>
                    <tr><th>Team Work</th></tr>
                    </thead>
                    {colors()}
                </tr>
                <tr>
                    <thead>
                    <tr><th>Content</th></tr>
                    </thead>
                    {colors()}
                </tr>
                </tbody>
            </table>

            <p>{JSON.stringify(data)}</p>
        </>
    );
}