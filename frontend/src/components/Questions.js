import React, {useState} from 'react';
import { useHistory } from "react-router-dom";

import {saveData} from "../ApiHelper";

export default function Questions() {
    const [name, setName] = useState('');
    const [nestedState, setNestedState] = useState([]);
    const [easyRelease, setEasyRelease] = useState([]);
    const [fun, setFun] = React.useState([]);
    const [codeHealth, setCodeHealth] = useState([]);
    const [learning, setLearning] = useState([]);
    const [vision, setVision] = useState([]);
    const [players, setPlayers] = useState([]);
    const [speed, setSpeed] = useState([]);
    const [suitableProcess, setSuitableProcess] = useState([]);
    const [support, setSupport] = useState([]);
    const [teamWork, setTeamWork] = useState([]);
    const [content, setContent] = useState([]);
    const [next, setNext] = useState(false);

    let history = useHistory();

    let formData =
        {
            name: {...name},
            delivery: {...nestedState},
            easyRelease: {...easyRelease},
            fun: {...fun},
            codeHealth: {...codeHealth},
            learning: {...learning},
            vision: {...vision},
            players: {...players},
            speed: {...speed},
            suitableProcess: {...suitableProcess},
            support: {...support},
            teamWork: {...teamWork},
            content: {...content},

        };

    const submitForm = () => {
        saveData(formData)
            .then(response => {
                history.push('/results');
                console.log('Success')
            }).catch(error => {
            console.log("error send form", error)
        });
    }

    const colors = (i) => {
        let color = ['green', 'yellow', 'red'];
        return (
            <>
                {color.map((item, index) => {
                    return <>
                        <td key={index}><input type="radio" name="color" value={item}
                                   onChange={e => {
                                       e.persist();
                                       i(color => ({...color, [e.target.name]: e.target.value}));
                                   }}/>
                        </td>
                    </>
                })}
                <td>
                    <input type="text" name="comment" placeholder="Comment here"
                           onChange={e => {
                               e.persist();
                               i(comment => ({...comment, [e.target.name]: e.target.value}));
                           }}/>
                </td>
            </>
        )
    }

    return (
        <div className="App">
        <form onSubmit={submitForm}>
            {!next && (
                <>
                <label>What's your name? </label>
                <input type="text" name="name" placeholder="Name"
                onChange={e => {
                e.persist();
                setName(name => ({...name, [e.target.name]: e.target.value}));
            }}/>
                <button className="button" type="button" onClick={() => name ? setNext(true)  : null}>Next</button>

                </>
            )}
            {next && (
                <div className="App">
                    <table className="App">
                        <thead>
                        <tr>
                            <th></th>
                            <th><buton type="button" className="green">Green</buton></th>
                            <th><buton type="button" className="yellow">Yellow</buton></th>
                            <th><buton type="button" className="red">Red</buton></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <thead>
                            <tr><th>Delivering Value</th></tr>
                            </thead>
                            {colors(setNestedState, nestedState)}
                        </tr>
                        <tr>
                            <thead>
                            <tr><th>Easy Release</th></tr>
                            </thead>
                            {colors(setEasyRelease)}
                        </tr>
                        <tr>
                            <thead>
                            <tr><th>Fun</th></tr>
                            </thead>
                            {colors(setFun)}
                        </tr>
                        <tr>
                            <thead>
                            <tr><th>Code Health</th></tr>
                            </thead>
                            {colors(setCodeHealth)}
                        </tr>
                        <tr>
                            <thead>
                            <tr><th>Learning</th></tr>
                            </thead>
                            {colors(setLearning)}
                        </tr>
                        <tr>
                            <thead>
                            <tr><th>Vision/Mission</th></tr>
                            </thead>
                            {colors(setVision)}
                        </tr>
                        <tr>
                            <thead>
                            <tr><th>Pawns/Players</th></tr>
                            </thead>
                            {colors(setPlayers)}
                        </tr>
                        <tr>
                            <thead>
                            <tr><th>Speed</th></tr>
                            </thead>
                            {colors(setSpeed)}
                        </tr>
                        <tr>
                            <thead>
                            <tr><th>Suitable Process</th></tr>
                            </thead>
                            {colors(setSuitableProcess)}
                        </tr>
                        <tr>
                            <thead>
                            <tr><th>Support</th></tr>
                            </thead>
                            {colors(setSupport)}
                        </tr>
                        <tr>
                            <thead>
                            <tr><th>Team Work</th></tr>
                            </thead>
                            {colors(setTeamWork)}
                        </tr>
                        <tr>
                            <thead>
                            <tr><th>Content</th></tr>
                            </thead>
                            {colors(setContent)}
                        </tr>
                        </tbody>
                    </table>
                    <button className="button"  type="button" onClick={() => submitForm()}>Submit</button>
                </div>
                )
            }
            <p>{JSON.stringify(formData)}</p>
        </form>
        </div>
    );

}