const ApiBase = 'http://localhost:3001'

const request = (options) => {
    const headers = new Headers({
        'Content-Type': 'application/json'
    })

    const defaults = {headers: headers};
    options = Object.assign({}, defaults, options);

    return fetch(options.url, options)
        .then(res => res.json())
        // .then(result => console.log(result))
        .catch(error => console.log('error', error));
};

export function getData() {

    return request({
        url: ApiBase + "/getData" ,
        method: 'GET'
    });
}

export function saveData(data) {
    return request({
        url: ApiBase + "/sendData/",
        method: 'POST',
        body: JSON.stringify(data)
    });
}






