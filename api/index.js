const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const port = process.env.PORT || 3001;

app.use(cors());

app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
);

//create Json
let dataJson = [];

app.post('/sendData', (req, res) => {
    const data = req.body;
    console.log(data);
    dataJson.push(data);
    res.send('data is added database');

});

app.get('/getData', (req, res) => {
    res.json(dataJson);
})

app.listen(port, () => console.log(`App listening on port ${port}!`));