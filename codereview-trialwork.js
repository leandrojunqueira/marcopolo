const ddbUtils = require('ddb-utils');
const axios = require('axios');
const countryMapping = {
    AND: 'AD',
    ARE: 'AE',
    AFG: 'AF',
    ATG: 'AG',
    DEU: 'DE'
};

exports.handler = async (event) => {
    try {
        const order = convertOrderStructure(event);
        await ddbUtils.bulkInsert('development-orders', [order]);
    } catch (error) {
        console.log(JSON.stringify(event));
        throw error;
    }
};

function convertOrderStructure(event) {
    const order = event.order;
    const locale = event.locale;
    const headData = getHead(order, locale);
    const data = getData(order);
    const positions = fetchPositions(order);
    const returnData = Object.assign(headData, data, positions);
    return returnData;
}

function fetchPositions(order) {
    return {
        positions: order.items.map((i, index) => {
            let isProductOnSale = false;
            if (i.category === 'sale') {
                isProductOnSale = true;
            }

            //Add response for promise updateStock success or fail could be useful
            updateStock({quantity: i.quantity}) ;

            //If it'd not supposed to change each "100 * 1.19" It could be added as a let/or const xPrice = 100 * 1.19
            return {
                grossPrice: i.price / 100 * 1.19,
                isSale: isProductOnSale,
                position: ++index,
                productId: i.ean,
                productType: getProductType(i.type),
                quantity: i.quantity,
                salePercent: i.price.appliedReductions.reduce((total, reduction) => total + reduction.amount, 0) / 100 * 1.19,
                tax: i.price.tax.vat.amount / 100 * 1.19,
                totalPrice: i.price * i.quantity / 100 * 1.19,
                productName: i.name,
                taxRate: i.price.tax.vat.rate
            };
        })
    };
}

function getProductType(type) {
    let productType = 'unknown';
    if (type === 'gc') {
        productType = 'giftcard';
    } else if (type === 'ga') {
        productType = 'giveaway';
    } else if (type === 'dm') {
        productType = 'denim';
    } else if (type === 'pr') {
        productType = 'pure';
    } else if (type === 'cs') {
        productType = 'casual';
    }
    return productType;
}

function getHead(order, locale) {
    return {
        countryId: locale.countryCode in countryMapping ? countryMapping[locale.countryCode] : undefined,
        crmCustomerId: order.customer.referenceKey,
        currency: locale.currencyCode,
        customerCardNumber: order.customer.publicKey,
        customerEmail: order.customer.email,
        locale: locale.languageCode,
        orderDate: new Date(order.confirmedAt).toISOString(),
        orderNo: order.id.toString(),
        totalGrossPrice: order.cost.withTax
    };
}

async function updateStock(stockData) {
    const result = await axios.post(`${process.env.mapiEndpoint}stocks`, stockData, {
        headers: {
            'Content-Type': 'application/json',
            'X-Access-Token': process.env.mapiAccessToken,
            'X-Merchant-Key': process.env.mapiMerchantKey
        }
    });
    return result;
}

function getData(order) {
    if (order.cost.appliedFees) {
        const shippingFees = getShippingFees(order);
        if (shippingFees !== null) {
            return {
                shippingTotalGrossPrice: shippingFees.amount.withTax / 100,
                shippingTotalNetPrice: shippingFees.amount.withoutTax / 100,
                shippingTotalTax: shippingFees.tax.vat.amount / 100
            };
        } else {
            return {
                shippingTotalGrossPrice: 0,
                shippingTotalNetPrice: 0,
                shippingTotalTax: 0
            };
        }
    } else {
        return {
            shippingTotalGrossPrice: 0,
            shippingTotalNetPrice: 0,
            shippingTotalTax: 0
        };
    }
}


// Remove the commented line if it's not necessary

// function getBillingAddress(order) {
// 	const billingAddress = order.address.billing;
// 	return {
// 		billingAddress: {
// 			city: billingAddress.city,
// 			street: billingAddress.street,
// 			zip: billingAddress.zipCode,
// 			houseNumber: billingAddress.houseNumber,
// 			firstName: billingAddress.recipient.firstName,
// 			lastName: billingAddress.recipient.lastName,
// 			gender: billingAddress.recipient.gender.toUpperCase(),
// 			countryId: countryMapping[billingAddress.countryCode]
// 		}
// 	};
// }

function getShippingFees(temp) {
    let shippingFees = undefined;
    for (const index in temp.cost.appliedFees) {
        const fee = temp.cost.appliedFees[index];
        if (fee.category === 'delivery') {
            shippingFees = fee;
        }
    }
    return shippingFees;
}
